require 'rails_helper'

# Test suite for the SubscriptionItems controller
RSpec.describe OrdersController, type: :controller do
  include_context "stripe" do
    let!(:customer)   { create(:customer) }
    let!(:category) { create(:category) }
    let!(:product)    { create(:product, category: category) }
    let!(:product2)    { create(:product, category: category) }
    
    describe "GET index" do
      it "assigns @products" do
        4.times do 
          product.orders.create(customer_id: customer.id, price: product.price)
        end
        orders = product.orders.order(id: :desc)
        @current_user = customer
        get :index
        expect(assigns(:orders)).to eq(orders)
      end

      it "renders the index template" do
        @current_user = customer
        get :index
        expect(response).to render_template("index")
      end
    end

    describe "DELETE destroy" do
      it "cancel order" do
        order = product.orders.create(customer_id: customer.id, price: product.price)
        order = order.reload
        order.status = 'inactive'
        order.save
        @current_user = customer
        
        delete :destroy, id: order.id

        expect(order.reload.status).to eq('canceled')
      end   
   
  
      it "renders the index template" do
        @current_user = customer
        order = product.orders.create(customer_id: customer.id, price: product.price)

        delete :destroy, id: order.id
  
        expect(response).to redirect_to("/orders")
      end
    end
  end
end
require 'rails_helper'

# Test suite for the Products controller
RSpec.describe ProductsController, type: :controller do
  include_context "stripe" do
    let!(:categories) { create_list(:category, 5) }
    let!(:category) { categories.first }
    let!(:product)    { create(:product, category: category) }
    let!(:product2)    { create(:product, category: category) }
    let!(:customer)   { create(:customer) }

    describe "GET index" do
      it "assigns @products" do
        get :index
        expect(assigns(:categories)).to eq(categories)
      end

      it "renders the index template" do
        get :index
        expect(response).to render_template("index")
      end
    end  

    describe "GET show" do
      it "assigns @product" do
        get :show, id: product.id
        expect(assigns(:product)).to eq(product)
      end
      
      it "renders the index template" do
        get :show, id: product.id
        expect(response).to render_template("show")
      end
    end 

    describe "POST reorder" do
      it "order product" do
        plan = create(:plan, product: product)
        @current_user = customer

        post :reorder, product_id: product.id
        expect(@current_user.orders.count).to be(1)
      end
      
      it "renders the index template" do
        plan = create(:plan, product: product)
        @current_user = customer
        post :reorder, product_id: product.id
        expect(response).to redirect_to("/products")
      end
    end  

    describe "POST subscribe" do
      it "create subscription" do
        plan = create(:plan, product: product)
        @current_user = customer

        post :subscribe, product_id: product.id
        expect(@current_user.subscriptions.count).to be(1)
      end
      
      it "add to subscription" do
        plan = create(:plan, product: product)
        plan2 = create(:plan, product: product2)
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id, status: 'active')
        @current_user = customer
        
        post :subscribe, product_id: product2.id
        expect(@current_user.subscriptions.count).to be(1)
        expect(subs.subscription_items.count).to be(2)
      end

      it "renders the index template" do
        plan = create(:plan, product: product)
        @current_user = customer
        post :subscribe, product_id: product.id
        expect(response).to redirect_to("/products")
      end
    end  

    describe "DELETE unsubscribe" do
      it "cancel subscription" do
        plan = create(:plan, product: product)
        plan2 = create(:plan, product: product2)
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id, status: 'active')
        @current_user = customer

        delete :unsubscribe, product_id: product.id
        expect(@current_user.subscriptions.available.count).to be(0)
      end  

      it "cancel plan from subscription" do
        plan = create(:plan, product: product)
        plan2 = create(:plan, product: product2)
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id, status: 'active')
        subs.add_to_subscription(product2)
        si1 = subs.subscription_items.first
        si1.status = 'active'
        si1.stripe_id = 'si_test_0000'
        si2 = subs.subscription_items.last
        si2.status = 'active'
        si2.stripe_id = 'si_test_0001'
        @current_user = customer

        delete :unsubscribe, product_id: product.id
        expect(subs.subscription_items.available.count).to be(1)
      end   

      it "renders the index template" do
        @current_user = customer
        post :unsubscribe, product_id: product.id
        expect(response).to redirect_to("/products")
      end
    end  
  end  
end
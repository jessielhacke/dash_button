require 'rails_helper'

# Test suite for the Subscription controller
RSpec.describe SubscriptionsController, type: :controller do
  include_context "stripe" do
    let!(:customer)   { create(:customer) }
    let!(:category) { create(:category) }
    let!(:product)    { create(:product, category: category) }
    let!(:product2)    { create(:product, category: category) }
    let!(:subscriptions) { create_list(:subscription, 2, :active, customer: customer) }
    
  
    describe "GET index" do
      it "assigns @products" do
        @current_user = customer
        get :index
        expect(assigns(:subscriptions)).to eq(subscriptions.reverse)
      end

      it "renders the index template" do
        @current_user = customer
        get :index
        expect(response).to render_template("index")
      end
    end  

    describe "GET show" do
      it "assigns @product" do
        @current_user = customer
        get :show, id: subscriptions.first.id
        expect(assigns(:subscription)).to eq(subscriptions.first)
      end
      
      it "renders the index template" do
        @current_user = customer
        get :show, id: subscriptions.first.id
        expect(response).to render_template("show")
      end
    end  
    describe "DELETE destroy" do
      it "cancel subscription" do
        plan = create(:plan, product: product)
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id)
        si1 = subs.subscription_items.first
        @current_user = customer
      
        delete :destroy, id: subs.id
        expect(si1.reload.status).to eq('canceled')
        expect(subs.reload.status).to eq('canceled')
      end  

      it "cancel all plans from subscription" do
        plan = create(:plan, product: product)
        plan2 = create(:plan, product: product2)
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id, status: 'active')
        subs.add_to_subscription(product2)
        si1 = subs.subscription_items.first
        si1.status = 'active'
        si1.stripe_id = 'si_test_0000'
        si2 = subs.subscription_items.last
        si2.status = 'active'
        si2.stripe_id = 'si_test_0001'
        @current_user = customer
  
        delete :destroy, id: subs.id
        expect(subs.reload.status).to eq('canceled')
        expect(si1.reload.status).to eq('canceled')
        expect(si2.reload.status).to eq('canceled')
      end   

      it "renders the show template" do
        @current_user = customer
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id, status: 'active')

        delete :destroy, id: @current_user.subscriptions.first.id
        expect(response).to redirect_to("/subscriptions")
      end
    end
  end
end
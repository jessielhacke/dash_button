require 'rails_helper'

# Test suite for the SubscriptionItems controller
RSpec.describe SubscriptionItemsController, type: :controller do
  include_context "stripe" do
    let!(:customer)   { create(:customer) }
    let!(:category) { create(:category) }
    let!(:product)    { create(:product, category: category) }
    let!(:product2)    { create(:product, category: category) }

    describe "DELETE destroy" do
      it "cancel subscription if there is only one plan attached to it" do
        plan = create(:plan, product: product)
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id)
        si1 = subs.subscription_items.first
        @current_user = customer
      
        delete :destroy, id: si1.id
        expect(subs.subscription_items.available.count).to be(0)
        expect(si1.reload.status).to eq('canceled')
        expect(subs.reload.status).to eq('canceled')
      end   

      it "cancel one plan on subscription but keep subscription active" do
        plan = create(:plan, product: product)
        plan2 = create(:plan, product: product2)
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id, status: 'active')
        subs.add_to_subscription(product2)
        si1 = subs.subscription_items.first
        si1.update_attributes(status: 'active', stripe_id: 'si_test_0000')
        si2 = subs.subscription_items.last
        si2.update_attributes(status: 'active', stripe_id: 'si_test_0001')
        @current_user = customer
  
        delete :destroy, id: si1.id
        expect(subs.reload.status).to eq('active')
        expect(si1.reload.status).to eq('canceled')
        expect(si2.reload.status).to eq('active')
      end   
  
      it "renders the show template" do
        @current_user = customer
        plan = create(:plan, product: product)
        subs = product.subscriptions.create(customer_id: customer.id, category: category.id)
        si1 = subs.subscription_items.first

        delete :destroy, id: si1.id
  
        expect(response).to redirect_to("/subscriptions/#{subs.id}")
      end
    end
  end
end
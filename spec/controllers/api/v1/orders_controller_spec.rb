# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Orders api', type: :request do
  include_context "stripe" do
    let(:customer)   { create(:customer) }
    let(:category) { create(:category) }
    let(:product)    { create(:product, category: category) }
    let(:product_2)    { create(:product, category: category) }
    let(:subs)      { create(:subscription, :active, customer: customer) }
    let(:si1)   { create(:subscription_item, :active, stripe_id: 'si_0000000001', subscription: subs, product: product) }
    let(:si2)   { create(:subscription_item, :active, stripe_id: 'si_0000000001', subscription: subs, product: product_2) }


    let(:request_headers) do
      {
        'Accept'        => 'application/json',
        'Content-Type'  => 'application/json'
      }
    end

    let(:url) { '/api/v1/orders/create' }

    let(:data) do
      {
        object: {
          customer: customer.stripe_id,
          lines:{
            data:[
              {
                subscription_item: si1.stripe_id
              },
              {
                subscription_item: si2.stripe_id
              }
            ]
          }
        }
      }
    end

    before do
      post url, {data: data}, headers: request_headers
    end

    it 'create orders' do
      expect(customer.orders.count).to eq(2)
    end
  end
end

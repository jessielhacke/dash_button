# spec/factories/product.rb
FactoryBot.define do
  factory :product do
    name { FFaker::Product.product_name }
    description { FFaker::Lorem.phrase }
    price { rand(100..1500) }
    category
  end
end

# spec/factories/order.rb
FactoryBot.define do
  factory :order do
    price { 1000 }
    customer
    subscription_item
  end
end

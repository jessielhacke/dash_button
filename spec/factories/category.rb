# spec/factories/category.rb
FactoryBot.define do
  factory :category do
    name { FFaker::Product.product }
  end
end

# spec/factories/customer.rb
FactoryBot.define do
  factory :customer do
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
    email { FFaker::Internet.free_email }
    stripe_id { 'cus_00000000000000' }
  end
end

# spec/factories/subscription.rb
FactoryBot.define do
  factory :subscription do
    customer
    
    trait :active do
      status 'active'
    end

    trait :inactive do
      status 'inactive'
    end

    trait :canceled do
      status 'canceled'
    end
  end
end

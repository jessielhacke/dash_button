# spec/factories/subscription_item.rb
FactoryBot.define do
  factory :subscription_item do
    product
    subscription

    trait :active do
      status 'active'
    end

    trait :inactive do
      status 'inactive'
    end

    trait :canceled do
      status 'canceled'
    end
  end
end

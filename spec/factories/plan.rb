# spec/factories/plan.rb
FactoryBot.define do
  factory :plan do
    name { FFaker::Product.product_name }
    product
  end
end

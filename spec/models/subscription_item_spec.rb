require 'rails_helper'

# Test suite for the SubscriptionItem model
RSpec.describe SubscriptionItem, type: :model do
  # Validation tests
  it { should validate_presence_of(:product) }
  it { should validate_presence_of(:subscription) }
end

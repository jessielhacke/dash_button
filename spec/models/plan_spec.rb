require 'rails_helper'

# Test suite for the Plan model
RSpec.describe Plan, type: :model do
  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:product) }
end

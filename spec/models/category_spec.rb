require 'rails_helper'

# Test suite for the Category model
RSpec.describe Category, type: :model do
  # Validation tests
  it { should validate_presence_of(:name) }

end

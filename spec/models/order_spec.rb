require 'rails_helper'

# Test suite for the Order model
RSpec.describe Order, type: :model do
  # Validation tests
  it { should validate_presence_of(:price) }

end

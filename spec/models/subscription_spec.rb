require 'rails_helper'

# Test suite for the Subscription model
RSpec.describe Subscription, type: :model do
  # Validation tests
  it { should validate_presence_of(:customer) }
end

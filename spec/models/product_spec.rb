require 'rails_helper'

# Test suite for the Product model
RSpec.describe Product, type: :model do
  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:price) }
end

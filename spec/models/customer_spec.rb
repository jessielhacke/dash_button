require 'rails_helper'

# Test suite for the Customer model
RSpec.describe Customer, type: :model do
  # Validation tests
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:email) }

end

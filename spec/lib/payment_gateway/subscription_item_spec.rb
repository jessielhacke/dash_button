require 'rails_helper'

# Test suite for the SubscriptionItem Library
RSpec.describe PaymentGateway::SubscriptionItem do
  context 'create subscription item' do
    let(:data) do
      {
        subscription: 'sub_test_0000',
        plan: 'plan_test_0000'
      }
    end
    let(:missing_data) do
      {
        subscription: 'sub_test_0000',
        plan: 'fake_plan'
      }
    end
    let(:response) do
      {
        'id': 'si_test_0000'
      }
    end
    it_behaves_like "payment gateway create" do
      let(:stub_success) do
        stub_request(:post, "https://api.stripe.com/v1/subscription_items").
            to_return(status: 200, body: response.to_json, headers: {})
      end
      let(:stub_error) do
        stub_request(:post, "https://api.stripe.com/v1/subscription_items").
            and_raise('Error')
      end
    end
  end

  context 'cancel subscription item' do
    let(:data) {'si_test_0000'}

    let(:response_retrieve) do
      {
        'id': 'si_test_0000'
      }
    end

    let(:response) { {} } 
    it_behaves_like "payment gateway cancel" do
      let(:stub_retrieve) do
        # Retrieve Subscription Item
        stub_request(:get, "https://api.stripe.com/v1/subscription_items/si_test_0000").
                  to_return(status: 200, body: response_retrieve.to_json, headers: {})
      end
      let(:stub_success) do
        # Delete Subscription Item
        stub_request(:delete, "https://api.stripe.com/v1/subscription_items/si_test_0000?prorate=false").
                  to_return(status: 200, body: {}.to_json, headers: {})
      end
      let(:stub_error) do
        stub_request(:delete, "https://api.stripe.com/v1/subscription_items/si_test_0000?prorate=false").
            and_raise('Error')
      end
    end
  end
end
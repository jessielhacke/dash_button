require 'rails_helper'

# Test suite for the Plan library
RSpec.describe PaymentGateway::Plan do
  context 'create invoice' do
    let(:data) do
      {
        amount: 1500,
        interval: 'month',
        name: 'plan',
        currency: 'usd'
      }
    end
    let(:missing_data) do
      {
        amount: 1500,
        interval: 'month',
        currency: 'usd'
      }
    end
    
    let(:response) { {id: 'plan_test_0000'} }
    
    it_behaves_like "payment gateway create" do
      let(:stub_success) do
        stub_request(:post, "https://api.stripe.com/v1/plans").
            to_return(status: 200, body: response.to_json, headers: {})
      end
      let(:stub_error) do
        stub_request(:post, "https://api.stripe.com/v1/plans").
                and_raise('Error')
      end
    end
  end
end
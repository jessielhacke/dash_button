require 'rails_helper'

# Test suite for the Produc library
RSpec.describe PaymentGateway::Product do
  context 'create invoice' do
    let(:data) do
      {
        name: 'product',
        type: 'good',
        description: 'a item'
      }
    end
    let(:missing_data) do
      {
        type: 'good',
        description: 'a item'
      }
    end
    
    let(:response) { {} }
    
    it_behaves_like "payment gateway create" do
      let(:stub_success) do
        stub_request(:post, "https://api.stripe.com/v1/products").
            to_return(status: 200, body: response.to_json, headers: {})
      end
      let(:stub_error) do
        stub_request(:post, "https://api.stripe.com/v1/products").
                and_raise('Error')
      end
    end
  end
end
require 'rails_helper'

# Test suite for the Subscription Library
RSpec.describe PaymentGateway::Subscription do
  context 'create subscription' do
    let(:data) do
      {
        customer: 'sub_test_0000',
        items: [
          { subscription_item: 'si_test_0000' }
        ]
      }
    end
    let(:missing_data) do
      {
        customer: 'sub_test_0000',
        items: [ ]
      }
    end
    let(:response) do
      {
        'id': 'sub_test_0000'
      }
    end
    it_behaves_like "payment gateway create" do
      let(:stub_success) do
        stub_request(:post, "https://api.stripe.com/v1/subscriptions").
            to_return(status: 200, body: response.to_json, headers: {})
      end
      let(:stub_error) do
        stub_request(:post, "https://api.stripe.com/v1/subscriptions").
            and_raise('Error')
      end
    end
  end

  context 'cancel subscription' do
    let(:data) {'sub_test_0000'}

    let(:response_retrieve) do
      {
        'id': 'sub_test_0000'
      }
    end

    let(:response) { {} } 
    it_behaves_like "payment gateway cancel" do
      let(:stub_retrieve) do
        # Retrieve Subscription
        stub_request(:get, "https://api.stripe.com/v1/subscriptions/sub_test_0000").
                  to_return(status: 200, body: response_retrieve.to_json, headers: {})
      end
      let(:stub_success) do
        # Delete Subscription
        stub_request(:delete, "https://api.stripe.com/v1/subscriptions/sub_test_0000").
                  to_return(status: 200, body: {}.to_json, headers: {})
      end
      let(:stub_error) do
        stub_request(:delete, "https://api.stripe.com/v1/subscriptions/sub_test_0000").
            and_raise('Error')
      end
    end
  end
end
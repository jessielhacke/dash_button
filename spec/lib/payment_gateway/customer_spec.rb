require 'rails_helper'

# Test suite for the Customer library
RSpec.describe PaymentGateway::Customer do
  context 'create customer' do
    let(:data) do
      customer = create(:customer)
      CustomerSerializer.new(customer).as_json
    end

    let(:response) { {} }
    
    it "create customer" do
      stub_request(:post, "https://api.stripe.com/v1/customers").
                to_return(status: 200, body: response.to_json, headers: {})
      pg_data = described_class.create(data, 'card_token')
      expect(pg_data.to_json).to eq(response.to_json)
    end
    it "error while create customer" do
      stub_request(:post, "https://api.stripe.com/v1/customers").
                and_raise('Error')
      pg_data = described_class.create(data, 'card_token')
      expect(pg_data).to eq(nil)
    end
  end
end
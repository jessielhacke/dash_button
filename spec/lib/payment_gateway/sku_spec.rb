require 'rails_helper'

# Test suite for the SKU library
RSpec.describe PaymentGateway::Sku do
  context 'create invoice' do
    let(:data) do
      {
        product: 'prod_test_0000',
        price: 1500,
        currency: 'usd',
        inventory: {
          type: 'infinite'
        }
      }
    end
    let(:missing_data) do
      {
        price: 1500,
        currency: 'usd',
        inventory: {
          type: 'infinite'
        }
      }
    end
    
    let(:response) { {id: 'sku_test_0000'} }
    
    it_behaves_like "payment gateway create" do
      let(:stub_success) do
        stub_request(:post, "https://api.stripe.com/v1/skus").
            to_return(status: 200, body: response.to_json, headers: {})
      end
      let(:stub_error) do
        stub_request(:post, "https://api.stripe.com/v1/skus").
                and_raise('Error')
      end
    end
  end
end
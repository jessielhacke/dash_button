require 'rails_helper'

# Test suite for the Invoice library
RSpec.describe PaymentGateway::Invoice do
  context 'create invoice' do
    let(:data) do
      {
        customer: 'cus_test_0000',
        subscription: 'sub_test_0000'
      }
    end
    let(:missing_data) do
      {
        customer: 'fake_cus',
        subscription: 'sub_test_0000'
      }
    end
    let(:response) { {} }
    
    it_behaves_like "payment gateway create" do
      let(:stub_success) do
        stub_request(:post, "https://api.stripe.com/v1/invoices").
                to_return(status: 200, body: response.to_json, headers: {})
      end
      let(:stub_error) do
        stub_request(:post, "https://api.stripe.com/v1/invoices").
                and_raise('Error')
      end
    end
  end
end
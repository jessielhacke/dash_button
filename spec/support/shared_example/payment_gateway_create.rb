RSpec.shared_examples "payment gateway create" do   
  it 'should work' do
    stub_success
    
    pg_data = described_class.create(data)
    expect(pg_data.to_json).to eq(response.to_json)
  end

  it 'should raise an error and return nil' do
    stub_error
    
    pg_data = described_class.create(missing_data)
    expect(pg_data).to eq(nil)
  end
end
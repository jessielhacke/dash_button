RSpec.shared_context "stripe", :shared_context => :metadata do
  before(:each) do
    response_subs = {
      "id": "sub_test_0000",
      "object": "subscription",
      "items": {
        "object": "list",
        "data": [
          {
            "id": "si_test_0000",
            "object": "subscription_item",
            "plan": {
              "id": "plan_test_0000",
              "product": "prod_test_0000",
              "name": "fiji"
            },
            "subscription": "sub_test_0000"
          }
        ],
      },
      "plan": {
        "id": "plan_test_0000",
        "object": "plan",
        "product": "prod_test_0000",
        "name": "fiji"
      },
    }

    response_retrieve_subs = {
      "id": "sub_test_0000",
      "object": "subscription",
      "items": {
        "object": "list",
        "data": [
          {
            "id": "si_test_0000",
            "object": "subscription_item",
            "plan": {
              "id": "plan_test_0000",
              "object": "plan",
              "product": "prod_test_0000",
              "name": "fiji"
            },
            "subscription": "sub_test_0000"
          }
        ],
      },
      "plan": {
        "id": "plan_test_0000",
        "object": "plan",
        "product": "prod_test_0000",
      },
    }
    
    response_plan = {
      "id": "plan_test_0000",
    }

    response_delete_sub =  {
      "id": "sub_test_0000",
      "object": "subscription",
      "customer": "cus_test_0000",
      "items": {
        "object": "list",
        "data": [
          {
            "id": "si_test_0000",
            "object": "subscription_item",
            "plan": {
              "id": "plan_test_0000",
              "object": "plan",
              "product": "prod_test_0000",
              "name": "plan_test"
            },
            "subscription": "sub_test_0000"
          }
        ],
      },
      "plan": {
        "id": "plan_test_0000",
        "object": "plan",
        "product": "prod_test_0000",
        "name": "plan_test"
      }
    }

    response_si = {
      "id": "si_test_0000",
    }

    response_retrieve_si = {
      "id": "si_test_0000",
      "object": "subscription_item",
    }

    response_prod = {
      "id": "prod_test_0000",
    }

    response_sku = {
      "id": "sku_test_0000",
    }

    # Create Subscription
    stub_request(:post, "https://api.stripe.com/v1/subscriptions").
            to_return(status: 200, body: response_subs.to_json, headers: {})
    
    # Retrieve Subscription
    stub_request(:get, "https://api.stripe.com/v1/subscriptions/sub_test_0000").
            to_return(status: 200, body: response_retrieve_subs.to_json, headers: {})
    
    # Delete Subscription
    stub_request(:delete, "https://api.stripe.com/v1/subscriptions/sub_test_0000").
            to_return(status: 200, body: response_delete_sub.to_json, headers: {})

    # Create Plan
    stub_request(:post, "https://api.stripe.com/v1/plans").
            to_return(status: 200, body: response_plan.to_json, headers: {})
            
    # Create Subscription Item
    stub_request(:post, "https://api.stripe.com/v1/subscription_items").
            to_return(status: 200, body: response_si.to_json, headers: {})

    # Retrieve Subscription Item
    stub_request(:get, "https://api.stripe.com/v1/subscription_items/si_test_0000").
            to_return(status: 200, body: response_retrieve_si.to_json, headers: {})

    # Delete Subscription Item
    stub_request(:delete, "https://api.stripe.com/v1/subscription_items/si_test_0000?prorate=false").
            to_return(status: 200, body: {}.to_json, headers: {}) 

    # Create Invoice
    stub_request(:post, "https://api.stripe.com/v1/invoices").
            to_return(status: 200, body: {}.to_json, headers: {})

    # Create Product
    stub_request(:post, "https://api.stripe.com/v1/products").
            to_return(status: 200, body: response_prod.to_json, headers: {})

    # Create SKU
    stub_request(:post, "https://api.stripe.com/v1/skus").
            to_return(status: 200, body: response_sku.to_json, headers: {})
  end
end
Rails.application.routes.draw do

  root to: 'home#index', via: :get

  resources :products, only: ['index', 'show'] do
    post '/reorder', to: 'products#reorder'
    post '/subscribe', to: 'products#subscribe'
    delete '/unsubscribe', to: 'products#unsubscribe'
  end
  
  resources :subscriptions, only: [:show, :index, :destroy]
  resources :subscription_items, only: [:destroy]
  resources :orders, only: [:index, :destroy]

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :orders, only: [] do 
        collection do
          post 'create', to: 'orders#create'
        end
      end
    end
  end

end

module PaymentGateway
  class Invoice
    def self.create(subscription)
      begin
        Stripe::Invoice.create(
          customer: subscription[:customer_id],
          subscription: subscription[:payment_gateway_id]
        )
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end
  end
end
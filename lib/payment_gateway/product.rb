module PaymentGateway
  class Product
    def self.create(product)
      begin
        Stripe::Product.create(
          name: product[:name],
          type: 'good',
          description: product[:description]
        )
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end
  end
end
module PaymentGateway
  class SubscriptionItem
    def self.create(item)
      begin
        Stripe::SubscriptionItem.create(
          subscription: item[:subscription_id], 
          plan: item[:plan_id]
        )
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end

    def self.cancel(si_pg_id)
      begin
        si = Stripe::SubscriptionItem.retrieve(si_pg_id)
        si.delete(prorate: false)
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end
  end
end
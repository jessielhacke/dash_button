module PaymentGateway
  class Customer
    def self.create(customer, card_token)
      begin
        Stripe::Customer.create(
          card: card_token,
          description: customer[:id],
          email: customer[:email],
          shipping: {
            name: customer[:name],
            address: {
              line1: customer[:line1],
              city: customer[:city],
              state: customer[:state],
              country: customer[:country],
              postal_code: customer[:postal_code]
            }
          }
        )
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end
  end
end
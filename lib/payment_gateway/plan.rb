module PaymentGateway
  class Plan
    def self.create(plan)
      begin
        Stripe::Plan.create(
          amount: plan[:price],
          interval: plan[:interval],
          name: plan[:name],
          currency: plan[:currency]
        )
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end
  end
end
module PaymentGateway
  class Order
    def self.create(order)
      begin
        Stripe::Order.create(
          currency: order[:currency],
          customer: order[:customer],
          items: [
            {
              type: 'sku',
              parent: order[:sku]
            }
          ]
        )
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end
  end
end
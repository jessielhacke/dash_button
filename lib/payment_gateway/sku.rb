module PaymentGateway
  class Sku
    def self.create(sku)
      begin
        Stripe::SKU.create(
          product: sku[:product],
          price: sku[:price],
          currency: sku[:currency],
          inventory: {
            type: sku[:inventory]
          }
        )
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end
  end
end
module PaymentGateway
  class Subscription
    def self.create(subscription)
      begin
        Stripe::Subscription.create(
          customer: subscription[:customer_id],
          items: subscription[:plans]
        )
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end

    def self.cancel(subscription_pg_id)
      begin
        subscription = Stripe::Subscription.retrieve(subscription_pg_id)
        subscription.delete
      rescue => e
        puts "EXCEPTION: #{e.message}"
        nil
      end
    end
  end
end
class OrdersController < ApplicationController
  
  before_action :set_current_user
  
  def index
    @orders = @current_user.orders.order(id: :desc)
  end

  def destroy
    @order = Order.find(params[:id])
    respond_to do |format|
      if @order.cancel_order
        format.html { redirect_to orders_url, notice: I18n.t('orders.canceled') }
      else
        flash[:error] =  @order.errors.full_messages.first
        format.html { redirect_to orders_url }
      end
    end
  end

end

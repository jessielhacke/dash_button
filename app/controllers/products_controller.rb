class ProductsController < ApplicationController
  
  before_action :set_current_user
  
  def index
    @categories = Category.all
  end

  def show
    @product = Product.find(params[:id])
  end
  
  def subscribe
    product = Product.find(params[:product_id])
    subscription_by_category = @current_user.subscriptions.available.by_category(product.category_id).first
    if subscription_by_category.present?
      respond_to do |format|
        if subscription_by_category.add_to_subscription(product)
          format.html { redirect_to products_url,
                        notice: I18n.t('subscriptions.plan_added',
                                       product: product.name,
                                       category: product.category.name) }
        else
          flash[:error] = subscription_by_category.errors.full_messages.first
          format.html { redirect_to products_url }
        end
      end
    else
      product.subscriptions.create(customer_id: @current_user.id, category: product.category_id)
      respond_to do |format|
        format.html { redirect_to products_url, 
                      notice: I18n.t('subscriptions.created', 
                                     product: product.name,
                                     category: product.category.name) }
      end
    end
  end

  def unsubscribe
    subscription = @current_user.subscriptions
                                 .available
                                 .joins(:subscription_items)
                                 .find_by(subscription_items: {product_id: params[:product_id]})
    respond_to do |format|
      if subscription.present?                             
        subscription_item = subscription.subscription_items.find_by(product_id: params[:product_id])
        if subscription_item.cancel_subscription_item
          format.html { redirect_to products_url, notice: I18n.t('subscriptions.canceled') }
        else
          flash[:error] = I18n.t('subscriptions.errors.cancel')
          format.html { redirect_to products_url }
        end
      else
        flash[:error] = I18n.t('subscriptions.errors.no_subscription')
        format.html { redirect_to products_url }
      end
    end
  end

  def reorder
    product = Product.find(params[:product_id])
    respond_to do |format|
      order = product.orders.create(customer: @current_user,
                                    price: product.price)
      if order.persisted?
        format.html { redirect_to products_url, notice: I18n.t('orders.created', product: product.name) }
      else
        flash[:error] = order.errors.full_messages.join(', ')
        format.html { redirect_to products_url }
      end
    end
  end
end

class SubscriptionsController < ApplicationController
  
  before_action :set_current_user
  
  def index
    @subscriptions = @current_user.subscriptions.order(id: :desc)
  end

  def show
    @subscription = @current_user.subscriptions.find(params[:id])
    @subscription_items = @subscription.subscription_items
    @orders = @subscription.orders
  end

  def destroy
    @subscription = Subscription.find(params[:id])
    respond_to do |format|
      if @subscription.cancel_subscription
        format.html { redirect_to subscriptions_url, notice: I18n.t('subscriptions.canceled') }
      else
        flash[:error] =  I18n.t('subscriptions.errors.cancel')
        format.html { redirect_to subscriptions_url }
      end
    end
  end
end

module Api
  module V1
    class OrdersController < ApplicationController
      protect_from_forgery with: :null_session

      def create
        customer_id = params['data']['object']['customer']
        customer = Customer.find_by(stripe_id: customer_id)
        response = []
        subscription_items = si_params(params['data']['object']['lines'])
        subscription_items['data'].each do |si_data|
          si = SubscriptionItem.find_by(stripe_id: si_data['subscription_item'])
          if si.present?
            customer.orders.create(subscription_item_id: si.id, 
                                   price: si.product.price, 
                                   product: si.product,
                                   status: Order::STATUS[:active])
          else
            response << "SubscriptionItem with id: #{si_data['subscription_item']} wasn't found!"
          end
        end
        render json: {message: response}, status: :ok
      end

      private

      def si_params(json)
        json_params = ActionController::Parameters.new(json)
        return json_params.permit(data: [:subscription_item])
      end
    end
  end
end
class SubscriptionItemsController < ApplicationController
  
  before_action :set_current_user
  
  def destroy
    @si = SubscriptionItem.find(params[:id])
    respond_to do |format|
      if @si.cancel_subscription_item
        format.html { redirect_to subscription_path(@si.subscription.id), notice: I18n.t('subscription_items.canceled') }
      else
        flash[:error] = I18n.t('subscription_items.errors.cancel')
        format.html { redirect_to subscription_path(@si.subscription.id) }
      end
    end
  end
end

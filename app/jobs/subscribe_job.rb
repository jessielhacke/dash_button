class SubscribeJob
  # Ideally the jobs should use Sidekiq with Redis
  include SuckerPunch::Job

  def perform(subscription_id)
    ActiveRecord::Base.connection_pool.with_connection do
      subscription = Subscription.find(subscription_id)
      if subscription.inactive?
        data = SubscriptionSerializer.new(subscription).as_json
        pg_subs = PaymentGateway::Subscription.create(data)
        # Return false if the PaymentGateway doesn't return the proper response
        return false if pg_subs.nil? || pg_subs[:id].nil?
        subscription.update_attributes(stripe_id: pg_subs[:id],
                                       status: Subscription::STATUS[:active])
        # For each subscription_item it will add stripe's id to the record
        pg_subs[:items][:data].each do |pg_si|
          si = data[:subscription_items].select{|d| d[:plan_id] == pg_si[:plan][:id]}.first
          local_si = SubscriptionItem.find(si[:id])
          local_si.update_payment_gateway_info(stripe_id: pg_si[:id], 
                                              status: SubscriptionItem::STATUS[:active])
        end
      end
    end
  end
end

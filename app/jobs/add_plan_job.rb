class AddPlanJob
  # Ideally the jobs should use Sidekiq with Redis
  include SuckerPunch::Job

  def perform(si_id)
    ActiveRecord::Base.connection_pool.with_connection do
      si = SubscriptionItem.find(si_id)
      if si.inactive?
        serialized_data = SubscriptionItemSerializer.new(si).as_json
        pg_subscription_item = PaymentGateway::SubscriptionItem.create(serialized_data)
        return false unless pg_subscription_item['id'].present?
        si.update_payment_gateway_info(stripe_id: pg_subscription_item['id'], 
                                       status: SubscriptionItem::STATUS[:active])
        subs_data = SubscriptionSerializer.new(si.subscription).as_json
        PaymentGateway::Invoice.create(subs_data)
      end
    end
  end
end

class CreateOrderJob
  # Ideally the jobs should use Sidekiq with Redis
  include SuckerPunch::Job

  def perform(order_id)
    ActiveRecord::Base.connection_pool.with_connection do
      order = Order.find(order_id)
      if order.inactive?
        pg_order = PaymentGateway::Order.create(OrderSerializer.new(order).as_json)
        return false unless pg_order['id'].present?
        order.update_attributes(stripe_id: pg_order['id'], 
                                status: Order::STATUS[:active])
      end
    end
  end
end

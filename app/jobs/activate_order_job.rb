class ActivateOrderJob
  # Ideally the jobs should use Sidekiq with Redis
  include SuckerPunch::Job

  def perform(order_id)
    ActiveRecord::Base.connection_pool.with_connection do
      order = Order.find(order_id)
      order.update_attributes(status: Order::STATUS[:active]) if order.inactive?
    end
  end
end

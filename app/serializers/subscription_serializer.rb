class SubscriptionSerializer < ActiveModel::Serializer
  attributes :plans, :customer_id, :subscription_items, :payment_gateway_id

  def subscription_items
    object.subscription_items.inactive.map {|d| SubscriptionItemSerializer.new(d).as_json}
  end

  def plans
    si = subscription_items
    si.map {|c| {plan: c[:plan_id]} } 
  end

  def customer_id
    object.customer.stripe_id
  end

  def payment_gateway_id
    object.stripe_id
  end
end
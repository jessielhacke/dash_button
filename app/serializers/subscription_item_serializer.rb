class SubscriptionItemSerializer < ActiveModel::Serializer
  attributes :id, :plan_id, :subscription_id

  def plan_id
    object.product.plan.stripe_id
  end

  def subscription_id
    object.subscription.stripe_id
  end
end
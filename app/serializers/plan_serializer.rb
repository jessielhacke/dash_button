class PlanSerializer < ActiveModel::Serializer
  attributes :name, :price, :product_name, :interval, :currency
  
  def price
    object.product.price
  end

  def product_name
    object.product.name
  end

  ## Constant attributes to be passed on
  def interval
    'month'
  end

  def currency
    'usd'
  end
end
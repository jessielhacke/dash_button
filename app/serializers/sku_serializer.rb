class SkuSerializer < ActiveModel::Serializer
  attributes :product, :price, :currency, :inventory
  
  def product
    object.stripe_id
  end

  def price
    object.price
  end

  def currency
    'usd'
  end

  def inventory
    'infinite'
  end
end
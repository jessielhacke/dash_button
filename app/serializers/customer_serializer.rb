class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :name, :email, :line1, 
             :city, :state, :country, :postal_code
  
  def name
    [object.first_name, object.last_name].join(' ')
  end
  
  # Fake Address
  def line1
    '1234 Main Street'
  end

  def city
    'New York City'
  end

  def state
    'NY'
  end

  def country
    'US'
  end

  def postal_code
    '12345'
  end

end

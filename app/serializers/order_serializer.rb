class OrderSerializer < ActiveModel::Serializer
  attributes :customer, :sku, :currency
  
  def customer
    object.customer.stripe_id
  end

  def sku
    object.product.stripe_sku_id
  end

  def currency
    'usd'
  end
end
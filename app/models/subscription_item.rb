class SubscriptionItem < ActiveRecord::Base
  include SubscriptionStatus

  attr_writer :extra_item

  belongs_to :subscription
  belongs_to :product
  has_many :orders

  validates :subscription, :product, presence: true

  enum status: STATUS

  after_create :add_to_payment_gateway, if: proc { extra_item == true }

  def extra_item
    @extra_item || false
  end

  def add_to_payment_gateway
    # Set the job to be performed after 1h, this way the user can cancel the
    # subscription if wanted
    # It's set to 1.minute for the sake of the tests
    AddPlanJob.perform_in(1.minute.to_i, self.id)
  end

  def cancel_subscription_item
    if self.subscription.subscription_items.available.count == 1
      self.subscription.cancel_subscription
    else
      if stripe_id.present?
        return false unless self.cancel_si_on_payment_gateway
      end
      self.update_attributes(status: STATUS[:canceled])
      true
    end
  end

  def cancel_si_on_payment_gateway
    PaymentGateway::SubscriptionItem.cancel(self.stripe_id)
  end

  def update_payment_gateway_info(pg_info={})
    self.update_attributes(pg_info)
  end
end

class Product < ActiveRecord::Base
  belongs_to :category
  has_one :plan
  has_many :subscription_items
  has_many :subscriptions, through: :subscription_items
  has_many :orders

  validates :name, :description, :price, :category, presence: true

  after_create :create_on_payment_gateway

  def create_on_payment_gateway
    pg_product = PaymentGateway::Product.create(ProductSerializer.new(self).as_json)
    self.update_attributes(stripe_id: pg_product['id']) 
    pg_sku = PaymentGateway::Sku.create(SkuSerializer.new(self).as_json)
    self.update_attributes(stripe_sku_id: pg_sku['id']) 
  end

  def image_path
    "dash_icons/#{self.category_name}/#{self.name}.jpg"
  end
  
  def category_name
    self.category.name
  end  
end

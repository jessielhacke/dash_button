class Plan < ActiveRecord::Base
  belongs_to :product
  
  after_create :create_plan_on_payment_gateway
  
  validates :product, :name, presence: true
  
  def create_plan_on_payment_gateway
    pg_plan = PaymentGateway::Plan.create(PlanSerializer.new(self).as_json)
    self.update_attributes(stripe_id: pg_plan['id']) 
  end  
end

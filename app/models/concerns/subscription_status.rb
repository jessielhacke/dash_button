module SubscriptionStatus
  extend ActiveSupport::Concern
  
  STATUS = {
    active:   'active',
    inactive: 'inactive',
    canceled: 'canceled' 
  }.freeze

  included do
    scope :active, (-> {where(status: STATUS[:active])})
    scope :available, (-> {where.not(status: STATUS[:canceled])})
    scope :inactive, (-> {where(status: STATUS[:inactive])})
    def available?
      !canceled?
    end
  end
end
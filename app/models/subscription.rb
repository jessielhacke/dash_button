class Subscription < ActiveRecord::Base
  include SubscriptionStatus
  
  # Accessor to simplify access to category on creating the object
  attr_accessor :category

  belongs_to :customer
  has_many :subscription_items
  has_many :products, through: :subscription_items
  has_many :orders, through: :subscription_items

  enum status: STATUS

  validates :customer, presence: true

  
  scope :by_category, ->(category_id) {joins(:products).where(products: {category_id: category_id})}
  scope :by_customer, ->(customer_id) {where(customer_id: customer_id)}

  after_commit :create_subscription_on_payment_gateway, on: :create

  after_update :cancel_subscription_items, if: proc {status_changed? && canceled?}
  validate :uniqueness_by_category
  

  def create_subscription_on_payment_gateway
    # Set the job to be performed after 6h, this way the user can cancel the
    # subscription if wanted
    # It's set to 1.minute for the sake of the tests
    SubscribeJob.perform_in(1.minute.to_i, self.id)
  end

  def cancel_subscription_on_payment_gateway
    PaymentGateway::Subscription.cancel(self.stripe_id)
  end

  def cancel_subscription
    if self.stripe_id.present? 
      return false if self.cancel_subscription_on_payment_gateway.nil?
    end
    self.update_attributes(status: STATUS[:canceled])
    true
  end

  def cancel_subscription_items
    self.subscription_items.each {|si| si.update_attributes(status: STATUS[:canceled])}
  end

  def uniqueness_by_category
    #uses accessor to get category passed when object is built
    category = self.category
    customer = self.customer_id
    if Subscription.available.by_customer(customer).by_category(category).present?
      errors[:base] << I18n.t('subscriptions.errors.is_not_unique')
    end
  end

  def add_to_subscription(product)
    extra_item = self.active?
    if self.subscription_items.available.exists?(product_id: product.id)
      errors[:base] << I18n.t('subscriptions.errors.plan_is_not_unique')
      return false
    else
      self.subscription_items.create(product_id: product.id, extra_item: extra_item)
    end
  end

  def get_category
    self.products.first.category
  end
end

class Order < ActiveRecord::Base
  include SubscriptionStatus

  belongs_to :subscription_item
  belongs_to :product
  belongs_to :customer
  
  enum status: STATUS

  validates :price, presence: true

  validate :only_order_one_per_day, on: :create, if: proc { self.subscription_item.nil? }

  after_create :activate_later, if: proc { self.subscription_item.nil? }

  def activate_later
    ActivateOrderJob.perform_in(1.minute.to_i, self.id)
  end

  #after_create :create_on_payment_gateway, if: proc { self.subscription_item.nil? }

  #def create_on_payment_gateway
    #CreateOrderJob.perform_in(1.minute.to_i, self.id)
  #end 

  def cancel_order
    if self.canceled? || self.active? 
      errors[:base] << I18n.t('orders.errors.is_active_or_canceled')
      return false
    end
    self.update_attributes(status: STATUS[:canceled])
    true
  end

  def only_order_one_per_day
    if self.product.present?
      today = DateTime.current
      orders = self.product.orders.available.where(customer_id: self.customer_id, 
                                                  created_at: today.beginning_of_day..today.end_of_day)
      if orders.any?
        errors.add(:product_id, I18n.t('orders.errors.is_not_unique_per_day'))
        false
      end
    end
  end
end

class Customer < ActiveRecord::Base
  has_many :subscriptions
  has_many :products, through: :subscriptions
  has_many :orders

  validates :first_name, :last_name, :email, presence: true

end

## Changes done

- Changed PaymentGateway to a separate Library to make it easier to scale if necessary
- Added serializers to be used when calling the new PaymentGateway Library, this way it becomes easier to use the Library in other applications or transform it into a Gem;
- Add stripe_id to Plan class
- Changed seed to create plan without sending the id and when it returns it adds the id from stripe to the Plan object.
- Add status ['active', 'inactive', 'canceled'] to Subscription and SubscriptionItem
- Add validation on subscription, check if unique by customer and category
- Add attr_accessor for Subscription class
- Add Error message on /locale/en.yml
- Add Unsubscribe to my subscriptions page
- Add canceled subscription
- Add cancelation of subscription on PaymentGateway
- Fix the layout
- Need to add deletion of subscription item and keep other but only when there is more than one
  - Add view to show subscription with subscription items
    - Add button on subscription/index.html to go to show view
  - On show vie use layout on paper
  - Add Controller actions to cancel subscription items
    - If only one SubscriptionItem on Subscription the subscription need to be canceled
  - Add button to products to cancel subscription, if more than one plan in subscription cancel only the one selected, if only one cancel the whole subscription
- Create model Order (SubscriptionItem)
- Create order (as paid) when API receive from Stripe Webhook the invoce.payment_suceed

## TODO

- Need to add Tests
  - Normal test to verifiy features


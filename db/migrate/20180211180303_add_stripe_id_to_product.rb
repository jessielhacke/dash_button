class AddStripeIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :stripe_id, :string
  end
end

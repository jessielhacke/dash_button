class AddStripeSkuIdToProduct < ActiveRecord::Migration
  def change
    add_column :products, :stripe_sku_id, :string
  end
end

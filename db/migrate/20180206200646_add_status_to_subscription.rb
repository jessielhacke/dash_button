class AddStatusToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :status, :string, default: 'inactive', index: true, null: false
  end
end

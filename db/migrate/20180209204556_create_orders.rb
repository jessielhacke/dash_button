class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :subscription_item, index: true, foreign_key: true
      t.references :customer, index: true, foreign_key: true, null: false
      t.references :product, index: true, foreign_key: true, null: false
      t.integer :price
      t.string :status, default: 'inactive'
      t.string :stripe_id

      t.timestamps null: false
    end
  end
end

class AddStatusToSubscriptionItem < ActiveRecord::Migration
  def change
    add_column :subscription_items, :status, :string, default: 'inactive', index: true, null: false

  end
end

== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.

## Changes done

- Changed PaymentGateway to a separate Library to make it easier to scale if necessary
- Added serializers to be used when calling the new PaymentGateway Library, this way it becomes easier to use the Library in other applications or transform it into a Gem;
- Add stripe_id to Plan class
- Changed seed to create plan without sending the id and when it returns it adds the id from stripe to the Plan object.
- Add status ['active', 'inactive', 'canceled'] to Subscription and SubscriptionItem
- Add validation on subscription, check if unique by customer and category
- Add attr_accessor for Subscription class
- Add Error message on /locale/en.yml
- Add Unsubscribe to my subscriptions page
- Add canceled subscription

- Need to add cancelation of subscription on PaymentGateway
- Need to add deletion of subscription item and keep other but only when there is more than one
- Need to add Tests
- Need to fix the layout

